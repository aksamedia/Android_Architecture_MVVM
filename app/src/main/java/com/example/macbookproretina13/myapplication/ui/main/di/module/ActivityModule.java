package com.example.macbookproretina13.myapplication.ui.main.di.module;

import com.example.macbookproretina13.myapplication.ui.main.activities.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract MainActivity contributeMainActivity();
}