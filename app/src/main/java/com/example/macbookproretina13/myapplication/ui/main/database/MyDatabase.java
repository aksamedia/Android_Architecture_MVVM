package com.example.macbookproretina13.myapplication.ui.main.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.example.macbookproretina13.myapplication.ui.main.database.converter.DateConverter;
import com.example.macbookproretina13.myapplication.ui.main.database.entity.User;
import com.example.macbookproretina13.myapplication.ui.main.database.dao.UserDao;

@Database(entities = {User.class}, version = 1)
@TypeConverters(DateConverter.class)
public abstract class MyDatabase extends RoomDatabase {

    // --- SINGLETON ---
    private static volatile MyDatabase INSTANCE;

    // --- DAO ---
    public abstract UserDao userDao();
}