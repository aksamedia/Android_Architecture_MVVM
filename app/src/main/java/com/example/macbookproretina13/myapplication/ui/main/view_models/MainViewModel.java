package com.example.macbookproretina13.myapplication.ui.main.view_models;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.macbookproretina13.myapplication.ui.main.database.entity.User;
import com.example.macbookproretina13.myapplication.ui.main.repositories.UserRepository;

import javax.inject.Inject;

public class MainViewModel extends ViewModel {
    private LiveData<User> user;
    private UserRepository userRepo;

    @Inject
    public MainViewModel(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    // ----

    public void init(String userId) {
        if (this.user != null) {
            return;
        }
        user = userRepo.getUser(userId);
    }

    public LiveData<User> getUser() {
        return this.user;
    }
}
