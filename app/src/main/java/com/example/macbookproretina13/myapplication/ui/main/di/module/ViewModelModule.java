package com.example.macbookproretina13.myapplication.ui.main.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.example.macbookproretina13.myapplication.ui.main.view_models.FactoryViewModel;
import com.example.macbookproretina13.myapplication.ui.main.view_models.MainViewModel;
import com.example.macbookproretina13.myapplication.ui.main.di.key.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindUserProfileViewModel(MainViewModel repoViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(FactoryViewModel factory);
}