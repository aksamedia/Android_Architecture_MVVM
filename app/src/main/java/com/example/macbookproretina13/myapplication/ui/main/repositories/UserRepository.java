package com.example.macbookproretina13.myapplication.ui.main.repositories;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.macbookproretina13.myapplication.ui.main.database.entity.User;
import com.example.macbookproretina13.myapplication.ui.main.api.Webservice;
import com.example.macbookproretina13.myapplication.ui.main.database.dao.UserDao;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Singleton
public class UserRepository {

    private static int FRESH_TIMEOUT_IN_MINUTES = 3;

    private final Webservice webservice;
    private final UserDao userDao;
    private final Executor executor;

    @Inject
    public UserRepository(Webservice webservice, UserDao userDao, Executor executor) {
        this.webservice = webservice;
        this.userDao = userDao;
        this.executor = executor;
    }

    // ---

    public LiveData<User> getUser(String userLogin) {
        refreshUser(userLogin); // try to refresh data if possible from Github Api
        return userDao.load(userLogin); // return a LiveData directly from the database.
    }

    // ---

    private void refreshUser(final String userLogin) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                // Check if user was fetched recently
                boolean userExists = (userDao.hasUser(userLogin, getMaxRefreshTime(new Date())) != null);
                // If user have to be updated
                if (!userExists) {
                    webservice.getUser(userLogin).enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(@NonNull Call<User> call, @NonNull final Response<User> response) {
                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    User user = response.body();
                                    if (user != null) {
                                        user.setLastRefresh(new Date());
                                        userDao.save(user);
                                    }
                                }
                            });
                        }

                        @Override
                        public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) { }
                    });
                }
            }
        });
    }

    // ---

    private Date getMaxRefreshTime(Date currentDate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.MINUTE, -FRESH_TIMEOUT_IN_MINUTES);
        return cal.getTime();
    }
}