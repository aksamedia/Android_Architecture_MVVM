package com.example.macbookproretina13.myapplication.ui.main.di.component;

import android.app.Application;


import com.example.macbookproretina13.myapplication.ui.main.di.module.ActivityModule;
import com.example.macbookproretina13.myapplication.ui.main.App;
import com.example.macbookproretina13.myapplication.ui.main.di.module.AppModule;
import com.example.macbookproretina13.myapplication.ui.main.di.module.FragmentModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by Philippe on 02/03/2018.
 */

@Singleton
@Component(modules={ActivityModule.class, FragmentModule.class, AppModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }

    void inject(App app);
}
