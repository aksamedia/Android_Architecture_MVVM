package com.example.macbookproretina13.myapplication.ui.main.di.module;

import com.example.macbookproretina13.myapplication.ui.main.fragments.MainFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract MainFragment contributeUserProfileFragment();
}